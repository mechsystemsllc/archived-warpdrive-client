mod utils;

use cfg_if::cfg_if;
use wasm_bindgen::prelude::*;

cfg_if::cfg_if! {
    // When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
    // allocator.
    if #[cfg(feature = "wee_alloc")] {
        #[global_allocator]
        static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;
    }
}

#[wasm_bindgen]
pub fn register(cls: String, url: String) -> bool {
    alert(format!("{} -> {} registered successfully", cls, url).as_str());
    true
}

#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
}
